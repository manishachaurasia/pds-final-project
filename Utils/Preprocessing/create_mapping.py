def create_Mapping(map_to, to_be_mapped, data_file="../Data/Crime_Data_from_2010_to_Present.csv"):
    data = pd.read_csv(data_file)
    mapped_dict = {}
    data_together = pd.DataFrame(trimmed_crime_data,columns = list([map_to,to_be_mapped]))
    unique_data = data_together.drop_duplicates()
    
    mapped_dict = {code:item for code,item in zip(unique_data[map_to],unique_data[to_be_mapped])}
    reverse_mapping = {item:code for code,item in zip(unique_data[map_to],unique_data[to_be_mapped])}
    
    return mapped_dict,reverse_mapping
