class Grid:
    def __str__(self):
        str1 = "({0} , {1} , {2} , {3})".format(self.left_lon,self.top_lat,self.right_lon,self.bottom_lat)
        # print(str1)
        return str1

    def __init__(self,left_lon,top_lat,right_lon,bottom_lat,x = None,y = None,name=None):
        """
        Initialize Area with 4 (lat,lon) co-ordinates representing
        Args:
            tl: Top Left co-ordinates - (lat,long)
            tr: Top Right co-ordinates - (lat,long)
            bl: Bottom Left co-ordinates - (lat,long)
            br: Bottom Right co-ordinates - (lat,long)
        """
        self.left_lon = left_lon
        self.top_lat = top_lat
        self.right_lon = right_lon
        self.bottom_lat = bottom_lat
        self.top_left = self.top_lat, self.left_lon
        self.top_right = self.top_lat , self.right_lon
        self.bottom_left = self.bottom_lat , self.left_lon
        self.bottom_right = self.bottom_lat , self.right_lon
        self.x = x
        self.y = y
        self.name = None

    def set_xy(self,x,y):
        self.x = x
        self.y = y

    def get_xy(self):
        return (self.x,self.y)

    def get_coordinates(self):
        """
        Returns: current Grid's co-ordinates (tl,tr,bl,br)
        """
        return (self.left_lon,self.top_lat,self.right_lon,self.bottom_lat)

    def set_coordinates(self,left_lon=None,top_lat=None,right_lon=None,bottom_lat=None):
        """
        Args:
            tl: Top Left co-ordinates - (lat,long)
            tr: Top Right co-ordinates - (lat,long)
            bl: Bottom Left co-ordinates - (lat,long)
            br: Bottom Right co-ordinates - (lat,long)

        Returns:
            NA
        """
        if left_lon:
            self.left_lon = left_lon
        if top_lat:
            self.top_lat = top_lat
        if right_lon:
            self.right_lon = right_lon
        if bottom_lat:
            self.bottom_lat = bottom_lat

    def get_name(self):
        """
        Returns:
            Name of the grid
        """
        return self.name

    def set_name(self,name):
        """
        Args:
            name: Name of the grid to set

        Returns:
            NA
        """
        self.name = name