from chunker import *

#LA coordinates
lat1 = 34.62
lon1 = 118.77
lat2 = 33.34
lon2 = 117.70

# lat1 = 33.8
# lat2 = 34.2
# lon1 = 118.0
# lon2 = 118.5

LA = AreaChunker(lon1,lat1,lon2,lat2)
LA.chunk(d=0.5)
# for (row, col) in LA.chunks:
#     print((row, col), LA.chunks[(row, col)])
row,col = LA.assign_point_to_chunks((33.9,118.2))
