from grid import Grid
from math import sin, cos, sqrt, atan2, radians , asin , pi
import math
import csv

class AreaChunker:

    def __init__(self,left_lon,top_lat,right_lon,bottom_lat):
        """
        Create root grid.
        Args:
            tl: Top Left co-ordinates - (lat,long)
            tr: Top Right co-ordinates - (lat,long)
            bl: Bottom Left co-ordinates - (lat,long)
            br: Bottom Right co-ordinates - (lat,long)
        """
        self.root = Grid(left_lon,top_lat,right_lon,bottom_lat)
        self.chunks = {}
        self.id = 0
        # print(self.root)


    def distance_between_points(self,A,B):
        """
        Args:
            lat1: Latitude of first co-ordinate
            lon1: Longitude of first co-ordinate
            lat2: Latitude of Second co-ordinate
            lon2: Longitude of first co-ordinate

        Returns:
            distance - distance between given co-ordinates in miles
        """

        # approximate radius of earth in km
        R = 6373.0

        lat1, lon1 = A
        lat2, lon2 = B

        lat1 = radians(lat1)
        lon1 = radians(lon1)
        lat2 = radians(lat2)
        lon2 = radians(lon2)

        dlon = lon2 - lon1
        dlat = lat2 - lat1

        a = sin(dlat / 2) ** 2 + cos(lat1) * cos(lat2) * sin(dlon / 2) ** 2
        c = 2 * atan2(sqrt(a), sqrt(1 - a))

        distance = R * c
        distance_in_miles = distance * 0.621371

        return distance_in_miles

    def new_coordinates_from_distance(self,coordinates,distance,brng,keep=2):
        """

        Args:
            coordinates: Co-ordinates from which to calculate new latitude and longitude
            distance:
            direction:

        Returns:

        """
        lat1,lon1 = coordinates
        R = 6378.1  # Radius of the Earth
        brng = math.radians(brng)  # Bearing is 90 degrees converted to radians.


        lat1 = math.radians(lat1)  # Current lat point converted to radians
        lon1 = math.radians(lon1)  # Current long point converted to radians

        lat2 = math.asin(math.sin(lat1) * math.cos(distance / R) +
                         math.cos(lat1) * math.sin(distance / R) * math.cos(brng))

        lon2 = lon1 + math.atan2(math.sin(brng) * math.sin(distance / R) * math.cos(lat1),
                                 math.cos(distance / R) - math.sin(lat1) * math.sin(lat2))

        lat2 = math.degrees(lat2)
        lon2 = math.degrees(lon2)

        if keep == 0:
            return lon2
        elif keep == 1:
            return lat2
        else:
            return (lat2,lon2)

    def chunk(self,d=0.5):
        """

        Args:
            d: Distance in km of each smaller grid square

        Returns:

        """
        start = self.root.top_left

        longitudes = [start[1]]
        while start[1] >= self.root.top_right[1]:
            west = self.new_coordinates_from_distance(start,d,270,keep=0)
            start  = (start[0],west)
            longitudes.append(west)
        start = self.root.top_left
        latitudes = [start[0]]


        while start[0] >= self.root.bottom_left[0]:
            south = self.new_coordinates_from_distance(start,d,180,keep=1)
            start  = (south,start[1])
            latitudes.append(south)

        print("Number of latitudes generated" ,len(latitudes))
        print("Number of longitudes generated" , len(longitudes))

        self.num_rows = len(latitudes)-1
        self.num_cols = len(longitudes)-1

        print(self.num_rows)
        print(self.num_cols)
        f = open("chunk_outline_data.csv","w")
        writer = csv.writer(f,delimiter=",")
        writer.writerow(["x","y","Left Longitude", "Right Longitude", "Top Latitude", "Bottom Latitude"])

        for long_idx in range(0,len(longitudes)-1):
            for lat_idx in range(0,len(latitudes)-1):
                x = lat_idx
                y = long_idx
                # print("here::",longitudes[long_idx],latitudes[lat_idx],longitudes[long_idx+1],latitudes[lat_idx+1])
                mini_chunk = Grid(longitudes[long_idx],latitudes[lat_idx],longitudes[long_idx+1],latitudes[lat_idx+1],x,y)

                la = [mini_chunk.top_lat,mini_chunk.top_lat,mini_chunk.bottom_lat,mini_chunk.bottom_lat]
                la = list(map(str,la))
                la = ",".join(la)

                lo = [-mini_chunk.left_lon,-mini_chunk.right_lon,-mini_chunk.left_lon,-mini_chunk.right_lon]
                lo = list(map(str, lo))
                lo = ",".join(lo)

                # row = [y,x,la,lo]
                row = [y,x,mini_chunk.left_lon,mini_chunk.right_lon,mini_chunk.top_lat,mini_chunk.bottom_lat]
                writer.writerow(row)
                self.chunks[(x,y)] = mini_chunk


        print("Number of chunks created :" , len(self.chunks))

    def assign_point_to_chunks(self,point,d=0.5):
        # print("left_lon:",self.chunks[(0,0)].left_lon," bottom_lat:",self.chunks[(0,0)].top_lat)

        for (row,col) in self.chunks:
            if point[0] < self.chunks[(row,col)].top_lat and point[0] > self.chunks[(row,col)].bottom_lat \
                and point[1] < self.chunks[(row,col)].left_lon and point[1] > self.chunks[(row,col)].right_lon:
                # print((row,col),self.chunks[(row,col)])
                print(row,col)
                return (row,col)